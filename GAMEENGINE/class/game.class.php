<?php
//include 'personnage.class.php';
class Game
{
    //*PROPRIETES

    //* domeDuTonnere
    public $_domeDuTonnere = [];

    //*METHODS
    //*construc test !!
    // public function __construct($j1, $j2, $j3)
    // {
    //     $domeDuTonnere = $this->_domeDuTonnere;
    //     array_push($domeDuTonnere, $j1, $j2, $j3);
    //     $this->_domeDuTonnere = $domeDuTonnere;
    // }
    //*getJoueur : retourne un joueur aléatoirement
    public function getJoueur($domeDuTonnere)
    {
        
       // var_dump($domeDuTonnere);
        $max = count($domeDuTonnere) - 1;
       // echo $max;
        $intAleatoire = mt_rand(0, $max);
        $joueur = $domeDuTonnere[$intAleatoire];
        $result = [$intAleatoire, $joueur];
        return $result;
    }
    //*tourDeJeu : effectue les actions de chaque tour
    public function tourDeJeu()
    {
        $domeDuTonnere = $this->_domeDuTonnere;
        //*choisir le joueur qui attaque => $first
       // var_dump($domeDuTonnere);
        $result = $this->getJoueur($domeDuTonnere);
        $first = $result[1];
       // var_dump($first);
        //*nv tableau sans le joueur choisi précedement
        $clé = $result[0];
        array_splice($domeDuTonnere, $clé, 1);
        $tableauAdversiares = $domeDuTonnere;
       // var_dump($tableauAdversiares);
        //*choix de la cible
        $result2 = $this->getJoueur($tableauAdversiares);
        $cible = $result2[1];
        //*method attaquer
        $first->attaquer($cible, $first->_talent, $first);
    }

    //*nettoyerMort : enleve un mort de l’arene si il y a en a un
    public function nettoyerMort()
    {
       $domeDuTonnere = $this->_domeDuTonnere;
        foreach ($domeDuTonnere as $key => $joueur) {
            if ($joueur->_pv <= 0) {
                echo $joueur->_nom . " est MORT !!\n";
                // echo $key."!!!!!!!!!!!!!!";
                array_splice($domeDuTonnere, $key, 1);
               // var_dump($domeDuTonnere);
            }
        }
        $this->_domeDuTonnere = $domeDuTonnere;
        return $this->_domeDuTonnere;
        //var_dump($this->_domeDuTonnere);
    }
    //*fin : retourne vrai si le combat est fini
    public function fin($fin)
    {
        $domeDuTonnere = $this->_domeDuTonnere;
        if (count($domeDuTonnere) === 1) {
            echo "Le combat est terminé !! Vainqueur: " . $domeDuTonnere[0]->_nom;
            return $fin = true;
        }else{
            return $fin = false;
        }
    }
}

//! Math random
//*mt_rand ( int $min , int $max ) : int
