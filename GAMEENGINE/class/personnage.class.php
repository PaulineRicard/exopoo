<?php

include '/home/pauline/ServeurWeb/exosPoo1/GAMEENGINE/interfaces/races.interface.php';
include '/home/pauline/ServeurWeb/exosPoo1/GAMEENGINE/interfaces/talents.interface.php';
class Personnage implements Races, Talents
{
    //* PROPRIETES
    public $_force;
    public $_pv;
    public $_endurance;
    public $_nom;
    public $_race;
    public $_talent;


    //*METHOD construct
    public function __construct($nom, $race, $talent) 
    {
        $this->_nom = $nom;
        $this->_race = $race;
        $this->setValues();
        $this->_talent =$talent;
    }

    //*METHOD elfe / humain / orc / setValues
    public function elfe()
    {
        $this->_force = 16;
        $this->_pv = 100;
        $this->_endurance = 4;
    }
    public function humain()
    {
        $this->_force = 12;
        $this->_pv = 100;
        $this->_endurance = 3;
    }
    public function orc()
    {
        $this->_force = 20;
        $this->_pv = 100;
        $this->_endurance = 1;
    }
    public function setValues()
    {
        if ($this->_race === 'elfe') {
            $this->elfe();
        } else if ($this->_race === 'humain') {
            $this->humain();
        } else if ($this->_race = 'orc') {
            $this->orc();
        }
    }

    //*METHOD attaquer
    public function attaquer($cible, $talent, $attaquant)
    {
        $intAleatoire = mt_rand(0, 4);
        if($intAleatoire === 2 && $talent === 'magicien'){
           $attaquant->mage($cible);
        }else if($intAleatoire === 2 && $talent === 'guerrier'){
            $attaquant->guerrier($cible);
        }else if($intAleatoire === 2 && $talent === 'guerisseur'){
            $attaquant->guerisseur($cible);
        }else{
            $vieCible = $cible->_pv;
            echo $this->_nom . " attaque " . $cible->_nom . "\n";
            $cible->_pv = $vieCible - $this->_force;
            echo "Points de vie de " . $cible->_nom ."-".$this->_force." = ". $cible->_pv . "\n";
        }
    }

    //*METHOD Mage/Guerrier/Guerisseur => "attaques" spéciales
    public function mage($cible)
    {
        $vieCible = $cible->_pv;
        $vieAttaquant = $this->_pv;
        echo "ATTAQUE SPECIALE Magicien->".$this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - ($this->_force)*2;
        $this->_pv = $vieAttaquant + 20;
        echo "Points de vie de " . $cible->_nom ."-".$this->_force." = ". $cible->_pv . "\n"."Régeneration mage: vie +20, points de vie ".$this->_nom." = ".$this->_pv."\n";
    }
    public function guerisseur($cible)
    {
        $vieCible = $cible->_pv;
        $vieAttaquant = $this->_pv;
        echo "ATTAQUE SPECIALE Guérisseur-> ".$this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - $this->_force;
        $this->_pv = $vieAttaquant + 20;
        echo "Points de vie de " . $cible->_nom ."-".$this->_force." = ". $cible->_pv . "\n"."Régeneration guerrisseur: vie +".(($this->_force)*2).", points de vie ".$this->_nom." = ".$this->_pv."\n";
    }
    public function guerrier($cible)
    {
        $vieCible = $cible->_pv;
        echo "ATTAQUE SPECIALE Guerrier-> ".$this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - ($this->_force)*3;
        echo "Points de vie de " . $cible->_nom ."-".$this->_force." = ". $cible->_pv . "\n";
    }
}
