<?php
include('/home/pauline/ServeurWeb/exosPoo1/GAMEENGINE/class/personnage.class.php');
include('/home/pauline/ServeurWeb/exosPoo1/GAMEENGINE/class/gameEngine.class.php');

//*creéation des persos
$j1 = new Personnage('joueur1', 'elfe', 'magicien');
$j2 = new Personnage('joueur2', 'humain', 'guerisseur');
$j3 = new Personnage('joueur3', 'orc', 'guerrier');

//*tableau des joueurs
$players = [];
//*rempli le tableau avec les persos crées
array_push($players, $j1, $j2, $j3);


//*instancie un nouvel objet GameEngine
$jeu = new GameEngine();
//* démarre le jeu instancier précedement
$jeu->start($players);