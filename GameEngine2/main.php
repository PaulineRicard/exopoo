<?php
include('autoload.php');

$players = [];

function creerObjetPersonnage($nomVariableObjet, $nom, $race, $talent)
{
    if ($race == 'elfe') :
        $nomVariableObjet = new Elfe($nom, $talent);
        //echo "Nouveau Personnage Elfe \n Nom: " . $nom . "\n Talent: " . $talent."\n";
        return $nomVariableObjet;
    elseif ($race == 'orc') :
        $nomVariableObjet = new Orc($nom, $talent);
        //echo "Nouveau Personnage Orc \n Nom: " . $nom . "\n Talent: " . $talent."\n";
        return $nomVariableObjet;
    elseif ($race == 'humain') :
        $nomVariableObjet = new Humain($nom, $talent);
        //echo "Nouveau Personnage Humain \n Nom: " . $nom . "\n Talent: " . $talent."\n";
        return $nomVariableObjet;
    else :

    endif;
}

$a = creerObjetPersonnage('player1', 'joueur1', 'elfe', 'guerrier');
$b = creerObjetPersonnage('player2', 'joueur2', 'elfe', 'guerisseur');
$c = creerObjetPersonnage('player3', 'joueur3', 'humain', 'magicien');
$d = creerObjetPersonnage('player4', 'joueur4', 'orc', 'guerrier');

array_push($players, $a, $b, $c, $d);
//*var_dump($players);

$jeu = new GameEngine();
$jeu->start($players);
//$jeu->addCombattant($players);
//var_dump($jeu->_domeDuTonnere);
//$jeu->tourDeJeuTournoi();
//$jeu->poolPremierTour();
//$jeu->tourDeJeuTournoi1();
//$pool = $jeu->_pool1;
//var_dump($pool);
//$jeu->duel($pool);