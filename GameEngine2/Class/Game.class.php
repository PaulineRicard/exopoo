<?php

class Game
{
    //!    //PROPRIETES

    public $_domeDuTonnere = [];
    public $_pool1 = [];
    public $_pool2 = [];
    public $_pool3 = [];
    public $_pool4 = [];



    //!    //*METHODS

    //todo getJoueur: retourne un joueur aléatoirement
    public function getJoueur($domeDuTonnere)
    {
        // var_dump($domeDuTonnere);
        $max = count($domeDuTonnere) - 1;
        // echo $max;
        $intAleatoire = mt_rand(0, $max);
        $joueur = $domeDuTonnere[$intAleatoire];
        $result = [$intAleatoire, $joueur];
        return $result;
    }

    //todo tourDeJeu : effectue les actions de chaque tour
    public function tourDeJeu()
    {
       $domeDuTonnere = $this->_domeDuTonnere;
        // var_dump($domeDuTonnere);

        //*choisi le joueur qui attaque => $first
        $result = $this->getJoueur($domeDuTonnere);
        $first = $result[1];
        // var_dump($first);

        //*nv tableau sans le joueur choisi précedement
        $clé = $result[0];
        array_splice($domeDuTonnere, $clé, 1);
        $tableauAdversiares = $domeDuTonnere;
        // var_dump($tableauAdversiares);

        //*choix de la cible
        $result2 = $this->getJoueur($tableauAdversiares);
        $cible = $result2[1];

        //*method attaquer
        $first->attaquer($cible, $first->_talent, $first);
    }
    //todo tourDeJeuTournoi : effectue les actions de chaque tour
    public function tourDeJeuTournoi1()
    {
        $pool1 = $this->_pool1;
        $pool2 = $this->_pool2;
    }
    //todo nettoyerMort : enleve un mort de l’arene si il y a en a un
    public function nettoyerMort()
    {
        $domeDuTonnere = $this->_domeDuTonnere;
        //*parcours le tableau des joueurs pour vérifier leur niveau de vie
        foreach ($domeDuTonnere as $key => $joueur) {
            //*si il est <= 0
            if ($joueur->_pv <= 0) {
                echo $joueur->_nom . " est MORT !!\n";
                // echo $key."!!!!!!!!!!!!!!";
                //*le joueur "mort" est retiré du tableau
                array_splice($domeDuTonnere, $key, 1);
                // var_dump($domeDuTonnere);
            }
        }
        //*met a jour le tableau sans le joueur mort dans l'objet
        $this->_domeDuTonnere = $domeDuTonnere;
        return $this->_domeDuTonnere;
        //var_dump($this->_domeDuTonnere);
    }
    //todo fin : retourne vrai si le combat est fini
    public function fin($fin)
    {
        $domeDuTonnere = $this->_domeDuTonnere;
        //*si le tableau des joueur ne contient qu'un seul joueur la partie est finie
        if (count($domeDuTonnere) === 1) {
            echo "Le combat est terminé !! Vainqueur: " . $domeDuTonnere[0]->_nom;
            return $fin = true;
        } else {
            return $fin = false;
        }
    }

    public function duel1($pool)
    {
        $j1 = $pool[0];
        $j2 = $pool[1];

        $duel = true; //* variable qui sera passée a false si un des joueurs "meurt"
        while ($duel === true) {
            if ($j1->_pv > 0) {
                $j1->attaquer($j2, $j1->_talent, $j1);
                if ($j2->_pv > 0) {
                    $j2->attaquer($j1, $j2->_talent, $j2);
                } else {
                    echo $j2->_nom . " est mort!!" . $j1->_nom . " à remporter le duel";
                    array_push($this->_pool3, $j1);
                    array_push($this->_pool4, $j2);
                    //var_dump($this->_pool3);
                    //var_dump($this->_pool4);
                    return $duel = false;
                }
            } else {
                echo $j1->_nom . " est mort!!" . " " . $j2->_nom . " à remporter le duel";
                array_push($this->_pool3, $j2);
                array_push($this->_pool4, $j1);
                //var_dump($this->_pool3);
                //var_dump($this->_pool4);
                return $duel = false;
            }
        }
    }

    //! faire fonction duel2 avec pool3 et pool4 ++ tableau final des joueur ++ affichage resultat 
    public function duel2($pool)
    {
        $j1 = $pool[0];
        $j2 = $pool[1];

        $duel = true; //* variable qui sera passée a false si un des joueurs "meurt"
        while ($duel === true) {
            if ($j1->_pv > 0) {
                $j1->attaquer($j2, $j1->_talent, $j1);
                if ($j2->_pv > 0) {
                    $j2->attaquer($j1, $j2->_talent, $j2);
                } else {
                    echo $j2->_nom . " est mort!!" . $j1->_nom . " à remporter le duel";
                    array_push($this->_pool3, $j1);
                    array_push($this->_pool4, $j2);
                    //var_dump($this->_pool3);
                    //var_dump($this->_pool4);
                    return $duel = false;
                }
            } else {
                echo $j1->_nom . " est mort!!" . " " . $j2->_nom . " à remporter le duel";
                array_push($this->_pool3, $j2);
                array_push($this->_pool4, $j1);
                //var_dump($this->_pool3);
                //var_dump($this->_pool4);
                return $duel = false;
            }
        }
    }

}
