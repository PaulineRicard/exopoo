<?php

class Personnage
{
    //*    //*PROPRIETES
    public $_nom;
    public $_talent;

    //*    //*METHOD
    public function __construct($nom, $talent)
    {
        $this->_nom = $nom;
        $this->_talent = $talent;
    }

    //*    //*METHOD attaquer
    public function attaquer($cible, $talent, $attaquant)
    {
        $intAleatoire = mt_rand(0, 4);
        if ($intAleatoire === 2 && $talent === 'magicien') {
            $attaquant->mage($cible);
        } else if ($intAleatoire === 2 && $talent === 'guerrier') {
            $attaquant->guerrier($cible);
        } else if ($intAleatoire === 2 && $talent === 'guerisseur') {
            $attaquant->guerisseur($cible);
        } else {
            $vieCible = $cible->_pv;
            echo $this->_nom . " attaque " . $cible->_nom . "\n";
            $cible->_pv = $vieCible - $this->_force;
            echo "Points de vie de " . $cible->_nom . "-" . $this->_force . " = " . $cible->_pv . "\n";
        }
    }
    //*METHOD Mage/Guerrier/Guerisseur => "attaques" spéciales
    public function mage($cible)
    {
        $vieCible = $cible->_pv;
        $vieAttaquant = $this->_pv;
        echo "ATTAQUE SPECIALE Magicien->" . $this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - ($this->_force) * 2;
        $this->_pv = $vieAttaquant + 20;
        echo "Points de vie de " . $cible->_nom . "-" . $this->_force . " = " . $cible->_pv . "\n" . "Régeneration mage: vie +20, points de vie " . $this->_nom . " = " . $this->_pv . "\n";
    }
    public function guerisseur($cible)
    {
        $vieCible = $cible->_pv;
        $vieAttaquant = $this->_pv;
        echo "ATTAQUE SPECIALE Guérisseur-> " . $this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - $this->_force;
        $this->_pv = $vieAttaquant + 20;
        echo "Points de vie de " . $cible->_nom . "-" . $this->_force . " = " . $cible->_pv . "\n" . "Régeneration guerrisseur: vie +" . (($this->_force) * 2) . ", points de vie " . $this->_nom . " = " . $this->_pv . "\n";
    }
    public function guerrier($cible)
    {
        $vieCible = $cible->_pv;
        echo "ATTAQUE SPECIALE Guerrier-> " . $this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - ($this->_force) * 3;
        echo "Points de vie de " . $cible->_nom . "-" . $this->_force . " = " . $cible->_pv . "\n";
    }
}

