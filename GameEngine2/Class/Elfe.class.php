<?php

class Elfe extends Personnage
{
    //*    //*PROPRIETES
    // Propriétés héritées de Personnage
    // public $_nom;
    // public $_talent;
    public $_force;
    public $_pv;
    public $_endurance;

    //*METHOD construct
    public function __construct($nom, $talent) 
    {
        $this->_nom = $nom;
        $this->_talent = $talent;
        $this->_force = 16;
        $this->_pv = 100;
        $this->_endurance = 4;
    }
}

//$elfe = new Elfe('elfe1', 'magicien');
//var_dump($elfe);