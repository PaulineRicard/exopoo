<?php

class GameEngine extends Game
{
    //*PROPRIETES
  
    //? public $_domeDuTonnere = [];  //? Hérité de Game


    //*METHODS

    //*  //*addCombattant(Personnage) => ajoute les Personnages au tableau de jeu
    public function addCombattant($players)
    {
        $this->_domeDuTonnere  = $players;
        // var_dump($this->_domeDuTonnere);
    }
   
    //*  //*start => Démarre le jeu
    public function start($players)
    {
        //*ajoute les perso crées au dome
        $this->addCombattant($players);
        //*variable de fin de jeu
        $fin = false;
            //*Tant que la partie n'est pas finie => continue la boucle while
            while ($fin != true) {
                //*lance un tour de jeu normal
                $domeDuTonnere = $this->_domeDuTonnere;
                $this->tourDeJeu();
                //*verifie si il y a des morts et les retire du jeu
                $this->nettoyerMort();
                $this->_domeDuTonnere =  $this->nettoyerMort();
                //   var_dump( $this->_domeDuTonnere);
                //*verifie si la partie est finie
                //$this->fin($fin);
                $fin =  $this->fin($fin);
            } 
    }
}
