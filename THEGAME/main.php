<?php

include 'THEGAME/class/personnage.class.php';

//*Création des personnages !!
$perso1 = new Personnage('P1', 'elfe');
$perso1->setValues();

$perso2 = new Personnage('P2', 'humain');
$perso2->setValues();

//*Tableau pour les duels
$domeDuTonnere = [];
array_push($domeDuTonnere, $perso1, $perso2);
var_dump($domeDuTonnere);

//*fonction DUEL
function duel($domeDuTonnere){
    $j1 = $domeDuTonnere[0];
    $j2 = $domeDuTonnere[1];
    $duel = true; //* variable qui sera passée a false si un des joueurs "meurt"
    while ($duel === true) {
        if($j1->_pv > 0){
            $j1->attaquer($j2);
            if($j2->_pv > 0){
                $j2->attaquer($j1);
            }else{
                echo $j2->_nom." est mort!!".$j1->_nom." à remporter le duel";
                return $duel = false;
            }
        }else{
            echo $j1->_nom." est mort!!"." ".$j2->_nom." à remporter le duel";
                return $duel = false;
        }
    }
}

duel($domeDuTonnere);