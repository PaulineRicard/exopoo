<?php

include('THEGAME/interfaces/race.interface.php');

class Personnage implements Race
{
    //* PROPRIETES
    public $_force;
    public $_pv;
    public $_endurance;
    public $_nom;
    public $_race;


    //*METHOD construct
    public function __construct($nom, $race)
    {
        $this->_nom = $nom;
        $this->_race = $race;
        
    }

    //*METHOD elfe / humain / orc / setValues
    public function elfe()
    {
        $this->_force = 16;
        $this->_pv = 100;
        $this->_endurance = 4;
    }
    public function humain()
    {
        $this->_force = 12;
        $this->_pv = 100;
        $this->_endurance = 3;
    }
    public function orc()
    {
        $this->_force = 20;
        $this->_pv = 100;
        $this->_endurance = 1;
    }
    public function setValues()
    {
        if ($this->_race === 'elfe') {
            $this->elfe();
        } else if ($this->_race === 'humain') {
            $this->humain();
        } else if ($this->_race = 'orc') {
            $this->orc();
        }
    }

    //*METHOD attaquer
    public function attaquer($cible)
    {
        $vieCible = $cible->_pv;
        echo $this->_nom . " attaque " . $cible->_nom . "\n";
        $cible->_pv = $vieCible - $this->_force;
        echo "Points de vie de " . $cible->_nom . " = " . $cible->_pv . "\n";
    }
}
