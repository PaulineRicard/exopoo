<?php
include('pied.php');
include('matiere.php');
class Meuble
{
    //*propriétés:
    // public $_couleur;
    // public $_nom;
    public $_mat;
    public $_hauteur;
    public $_nbPieds;

    //*method:
    // //*Constructeur!!
    function __construct()
        {
          //  $this->_couleur = $couleur;
         //   $this->_nom = $nom;
            $this->_mat = new Matiere();
            $this->_hauteur = new Pied();
            $this->_nbPieds = $nbPieds;
           //* $this->toString();
        }

    public function toString()
    {
        echo "J'ai " . $this->_nbPieds . " pieds d'une hauteur de " . $this->_hauteur . ", je suis composé de " . $this->_nom . " et je suis " . $this->_couleur . "\n";
    }
}

// $canap = new Meuble("noir", "cuir", "10cm", "4");
// //$canap->toString();

// $table = new Meuble("verte", "bois", "52cm", "4");
// //$table->toString();

// $meuble = new Meuble("rose", "bois", "15cm", "3");
// //$meuble->toString();

// $aa = new Meuble("11", "aa", "aa", "aa");

//! methode sans le constructeur
// $fauteuil = new Meuble();
// $fauteuil->_hauteur = '10cm';
// $fauteuil->_nbPieds = '4';
// $fauteuil->_nom = 'cuir';
// $fauteuil->_couleur = 'beige';
// $fauteuil->toString();

$meuble = new Meuble("4");
var_dump($meuble);