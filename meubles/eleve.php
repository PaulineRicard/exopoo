<?php

class Eleve
{
    // Propriétes
    public $_nom;
    public $_prenom;

    // Method
    public function afficheEleve()
    {
        echo $this->_nom ." ". $this->_prenom;
    }
}

$nouvelEleve = new Eleve();
$nouvelEleve->_nom = 'Ricard';
$nouvelEleve->_prenom = 'Pauline';

$nouvelEleve->afficheEleve();

